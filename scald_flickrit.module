<?php
/**
 * @file
 * Defines a Flickrit Gallery provider for Scald.
 */

define('SCALD_FLICKR_WEB_REGEXP', '#https?://www\.flickr\.com/photos/\S+/sets/([0-9]+)#');
define('SCALD_FLICKR_SET_REGEXP', '/^[0-9\-_]+$/');

/**
 * Implements hook_scald_atom_providers().
 */
function scald_flickrit_scald_atom_providers() {
  return array(
    'flickrit' => 'Flickrit Gallery',
  );
  // This code will never be hit, but is necessary to mark the string
  // for translation on localize.d.o
  t('Flickrit Gallery');
}

/**
 * Implements hook_scald_add_form().
 */
function scald_flickrit_scald_add_form(&$form, &$form_state) {
  $form['identifier'] = array(
    '#type' => 'textfield',
    '#title' => t('Flickr Gallery URL'),
    '#element_validate' => array('scald_flickrit_validate_id'),
    '#default_value' => '',
  );
}

/**
 * Implements hook_scald_add_form_fill().
 */
function scald_flickrit_scald_add_form_fill(&$atom, $form, $form_state) {
  // Get the identifier.
  $identifier = scald_flickrit_parse_id($form_state['values']['identifier']);
  $atom->base_id = $identifier;
}

/**
 * Implements hook_scald_fetch().
 */
function scald_flickrit_scald_fetch($atom, $type) {
  if ($type == 'atom') {
    $scald_thumbnail = field_get_items('scald_atom', $atom, 'scald_thumbnail');
    if (!empty($scald_thumbnail)) {
      $atom->thumbnail_source = $scald_thumbnail[0]['uri'];
    }
    else {
      $atom->thumbnail_source = drupal_get_path('module', 'scald_flickrit') . '/icons/flickrit-default.png';
    }
  }
}

/**
 * Implements hook_scald_prerender().
 */
function scald_flickrit_scald_prerender($atom, $context, $options, $mode) {
  if ($mode == 'atom') {
    if ($context === 'sdl_library_item') {
      $scald_thumbnail = field_get_items('scald_atom', $atom, 'scald_thumbnail');
      if (empty($scald_thumbnail)) {
        $atom->rendered->thumbnail_transcoded_url = file_create_url($atom->thumbnail_source);
      }
    }
    else {
      $config = scald_context_config_load($context);

      $options = array();
      if (isset($config->data['width']) && !empty($config->data['width'])) {
        $options['width'] = str_replace('%', '', $config->data['width']);
        if ($options['width'] > 100) {
          $options['width'] = 100;
        }
      }
      else {
        $options['width'] = 100;
      }

      if (isset($config->data['height']) && !empty($config->data['height'])) {
        $options['height'] = str_replace('%', '', $config->data['height']);
        if ($options['height'] > 200) {
          $options['height'] = 200;
        }
      }
      else {
        $options['height'] = 100;
      }

      $atom->rendered->player = theme('scald_flickrit_player',
        array(
          'set' => $atom->base_id,
          'atom' => $atom,
          'options' => $options,
        )
      );
    }
  }
}

/**
 * Form element validation handler for Flickr identifier.
 */
function scald_flickrit_validate_id($element, &$form_state) {
  $set = scald_flickrit_parse_id($form_state['values']['identifier']);
  if (!$set) {
    form_error($element, t('Invalid Flickr gallery identifier.'));
  }
}

/**
 * Parse a Flickr SET and check validity.
 */
function scald_flickrit_parse_id($string) {
  $set = NULL;
  $string = trim($string);

  if (!preg_match(SCALD_FLICKR_SET_REGEXP, $string)) {
    // The string ID is not easy to parse, let's try to analyze it.
    if (preg_match(SCALD_FLICKR_WEB_REGEXP, $string, $m)) {
      // The string ID is not easy to parse, let's try to analyze it.
      $set = $m[1];
    }
    elseif (preg_match("/^http/", $string)) {
      // This string is a URL, most likely a shortened one.
      // (http://dai.ly, http://bit.ly, etc...)
      $response = drupal_http_request($string);
      if ($response->code == 200 && isset($response->redirect_code) && ($response->redirect_code == 301 || $response->redirect_code == 302)) {
        return scald_flickrit_parse_id($response->redirect_url);
      }
    }
  }
  else {
    $set = $string;
  }

  return $set;
}

/**
 * Implements hook_theme().
 */
function scald_flickrit_theme() {
  return array(
    'scald_flickrit_player' => array(
      'variables' => array('set' => NULL, 'atom' => NULL, 'options' => NULL),
      'template' => 'scald_flickrit_player',
    ),
  );
}
