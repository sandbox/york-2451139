<?php
/**
 * @file
 * Default theme implementation for the Scald Flickrit Gallery Player.
 */
?>
<?php
  if (isset($options['width'])):
    $options['width'] = str_replace('%', '', $options['width']);
    if ($options['width'] > 100):
      $options['width'] = 100;
    endif;
  else:
    $options['width'] = 100;
  endif;

  if (isset($options['height'])):
    $options['height'] = str_replace('%', '', $options['height']);
    if ($options['height'] > 200):
      $options['height'] = 200;
    endif;
  else:
    $options['height'] = 100;
  endif;
?>
<div style='position: relative; padding-bottom: <?php echo $options['height']; ?>%; height: 0; width: <?php echo $options['width']; ?>%; overflow: hidden;'>
  <iframe id='iframe' src='http://flickrit.com/slideshowholder.php?height=<?php echo ($options['height'] - 1); ?>&size=big&setId=<?php echo $set; ?>&thumbnails=0&transition=0&layoutType=responsive&sort=0' scrolling='no' frameborder='0'style='width:100%; height:100%; position: absolute; top:0; left:0;' ></iframe>
</div>
